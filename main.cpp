//
// Created by tidemor on 4/25/2022.
//

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>
#include <list>
#include <string>
#include <chrono>
#include <functional>

class IllegalArgumentException : public std::exception {
public:
    explicit IllegalArgumentException(std::string msg = "IllegalArgumentException") : m_what(std::move(msg)) {}
    explicit IllegalArgumentException(const char* msg = "IllegalArgumentException") : m_what(std::string(msg)) {}
    [[nodiscard]] const char * what() const noexcept override { return m_what.c_str(); }

private:
    std::string m_what;
};

class InvalidStateException : public std::exception {
public:
    explicit InvalidStateException(std::string msg = "InvalidStateException") : m_what(std::move(msg)) {}
    explicit InvalidStateException(const char* msg = "InvalidStateException") : m_what(std::string(msg)) {}
    [[nodiscard]] const char * what() const noexcept override { return m_what.c_str(); }

private:
    std::string m_what;
};

class SlidingPuzzle {
public:
    explicit SlidingPuzzle(std::vector<int> state) : state(std::move(state)) { }

    [[nodiscard]] const std::vector<int>& GetState() const {
        return state;
    }

    [[nodiscard]] std::vector<std::vector<int>> GetMoves() const {
        std::vector<std::vector<int>> moves;

        auto n = std::find(state.begin(),  state.end(), 0);

        if(n == std::end(state))
            throw InvalidStateException("Cannot find empty Tile in state.");

        int i = n - state.begin();
        int x = i % 3;
        int y = i / 3;

        if(x > 0) {
            auto move = std::vector<int>(state);

            move[i] = state[i - 1];
            move[i - 1] = state[i];

            moves.emplace_back(move);
        }

        if(x < 2) {
            auto move = std::vector<int>(state);

            move[i] = state[i + 1];
            move[i + 1] = state[i];

            moves.emplace_back(move);
        }

        if(y > 0) {
            auto move = std::vector<int>(state);

            move[i] = state[i - 3];
            move[i - 3] = state[i];

            moves.emplace_back(move);
        }

        if(y < 2) {
            auto move = std::vector<int>(state);

            move[i] = state[i + 3];
            move[i + 3] = state[i];

            moves.emplace_back(move);
        }

        return moves;
    }

    [[nodiscard]] bool operator==(const SlidingPuzzle& other) {
        return state == other.state;
    }

    [[nodiscard]] std::string Print() const {
        std::string out =
                " %3d | %3d | %3d \n"
                "-----+-----+-----\n"
                " %3d | %3d | %3d \n"
                "-----+-----+-----\n"
                " %3d | %3d | %3d \n";

        int n = std::snprintf(nullptr, 0, out.c_str(), state[0], state[1], state[2], state[3], state[4],
                              state[5], state[6], state[7], state[8]);

        char format[n + 1];

        std::snprintf(format, n, out.c_str(), state[0], state[1], state[2], state[3], state[4],
                      state[5], state[6], state[7], state[8]);

        format[n] = '\0';

        return {format};
    }

private:
    const std::vector<int> state;
};

void IterativeDeepening(const std::vector<int>& initialState, const std::vector<int>& targetState, int maxDepth) {

    if(initialState.size() != 9 || targetState.size() != 9)
        throw IllegalArgumentException("States must be lists of 9 integers.");

    struct SlidingPuzzleHolder {
        SlidingPuzzle puzzle;
        SlidingPuzzleHolder* parent;

        explicit SlidingPuzzleHolder(std::vector<int> puzzle, SlidingPuzzleHolder* parent = nullptr)
                : puzzle({std::move(puzzle)}), parent(parent) { }
    };

    const SlidingPuzzleHolder* result = nullptr;

    auto nowTime = std::chrono::steady_clock::now;
    auto startTime = nowTime();

    auto isCycling = [](const SlidingPuzzleHolder* node) {

        const SlidingPuzzleHolder* runner = node->parent;
        while(runner) {
            if(runner->puzzle.GetState() == node->puzzle.GetState())
                return true;
            runner = runner->parent;
        }

        return false;
    };

    std::function<void(SlidingPuzzleHolder*, int, int)> _iterativeDeepening = [&](SlidingPuzzleHolder* head, int depth, int maxDepth) {

        if(head->puzzle.GetState() == targetState) {
            result = head;
            return;
        }

        if(depth > maxDepth)
            return;

        for (const auto &item : head->puzzle.GetMoves()) {
            auto child = new SlidingPuzzleHolder(item, head);

            if(!isCycling(child))
                _iterativeDeepening(child, depth + 1, maxDepth);

            if(result)
                return;

            delete child;
        }
    };

    for(int i = 0; i < maxDepth; i++) {
        auto root = new SlidingPuzzleHolder(initialState);
        _iterativeDeepening(root, 0, i);

        if(result)
            break;
    }

    if(result) {
        int n = -1;
        while (result) {
            std::cout << result->puzzle.Print() << "\n\n";
            auto tmp = result;
            result = result->parent;
            delete tmp;
            n++;
        }

        std::cout << "Iterative Deepening done in " << n << " steps and "
                  << std::chrono::duration_cast<std::chrono::milliseconds>((nowTime() - startTime)).count() << "ms."
                  << std::endl;
    } else {
        std::cout << "Iterative Deepening did not find a solution in " << maxDepth << " moves." << std::endl;
    }
}

void BreadthFirst(const std::vector<int>& initialState, const std::vector<int>& targetState) {

    if(initialState.size() != 9 || targetState.size() != 9)
        throw IllegalArgumentException("States must be lists of 9 integers.");

    struct SlidingPuzzleHolder {
        SlidingPuzzle puzzle;
        SlidingPuzzleHolder* parent;

        explicit SlidingPuzzleHolder(std::vector<int> puzzle, SlidingPuzzleHolder* parent = nullptr)
        : puzzle({std::move(puzzle)}), parent(parent) { }
    };

    std::list<SlidingPuzzleHolder*> search;
    std::list<SlidingPuzzleHolder*> done;

    search.push_back(new SlidingPuzzleHolder(initialState));

    SlidingPuzzleHolder* result = nullptr;

    auto nowTime = std::chrono::steady_clock::now;
    auto startTime = nowTime();

    while(!search.empty()) {

        std::for_each(search.begin(), search.end(), [&](SlidingPuzzleHolder*& item){
            if(item->puzzle.GetState() == targetState)
                result = item;
        });

        if(result)
            break;

        std::list<SlidingPuzzleHolder*> next;

        for (SlidingPuzzleHolder*& item : search) {
            done.push_back(item);
            for (auto &move : item->puzzle.GetMoves()) {
                next.push_back(new SlidingPuzzleHolder(move, item));
            }
        }

        search = next;
    }

    int n = -1;
    while (result) {
        std::cout << result->puzzle.Print() << "\n\n";
        result = result->parent;
        n++;
    }

    std::cout << "Breadth First Search done in " << n << " steps and "
              << std::chrono::duration_cast<std::chrono::milliseconds>((nowTime() - startTime)).count() << "ms."
              << std::endl;

    std::for_each(search.begin(), search.end(), [&](const auto& item){
        delete item;
    });

    std::for_each(done.begin(), done.end(), [&](const auto& item){
        delete item;
    });
}

void AStar(const std::vector<int>& initialState, const std::vector<int>& targetState) {

    if(initialState.size() != 9 || targetState.size() != 9)
        throw IllegalArgumentException("States must be lists of 9 integers.");

    auto h = [targetState](const SlidingPuzzle& game) {

        auto s = game.GetState();
        auto e = targetState;

        int out = 0;

        for(int i = 0; i < s.size(); i++) {
            int sx = i % 3;
            int sy = i / 3;

            int ei = std::find(e.begin(),  e.end(), s[i]) - e.begin();

            int ex = ei % 3;
            int ey = ei / 3;

            out += std::abs(sx - ex) + std::abs(sy - ey);
        }

        return out;
    };

    struct SlidingPuzzleNode {
        SlidingPuzzle puzzle;
        double distance;
        SlidingPuzzleNode* parent;

        explicit SlidingPuzzleNode(std::vector<int> puzzleState, double heuristic = 0, SlidingPuzzleNode* parent = nullptr)
            : puzzle(SlidingPuzzle(std::move(puzzleState))), distance(heuristic), parent(parent)
            { }

        bool operator==(const SlidingPuzzleNode& other) {

            bool same = (distance == other.distance);
            same = same && (parent == other.parent);
            same = same && (puzzle == other.puzzle);

            return same;
        }
    };

    std::list<SlidingPuzzleNode*> search;
    std::list<SlidingPuzzleNode*> done;

    search.emplace_back(new SlidingPuzzleNode(initialState));

    SlidingPuzzleNode* node = nullptr;

    auto nowTime = std::chrono::steady_clock::now;
    auto startTime = nowTime();

    while(!search.empty()) {
        node = search.front();
        std::for_each(search.begin(), search.end(), [&](SlidingPuzzleNode* i) { if( i->distance + h(i->puzzle) < node->distance + h(node->puzzle) ) node = i; });

        if(node->puzzle.GetState() == targetState)
            break;

        search.remove(node);
        done.push_back(node);

        for(const auto& child : node->puzzle.GetMoves()) {
            search.emplace_back(new SlidingPuzzleNode(child, node->distance + 1, node));
        }
    }

    if(!search.empty()) {
        int n = -1;
        while (node) {
            std::cout << node->puzzle.Print() << "\n\n";
            node = node->parent;
            n++;
        }

        std::cout << "A* done in " << n << " steps and "
                  << std::chrono::duration_cast<std::chrono::milliseconds>((nowTime() - startTime)).count() << "ms."
                  << std::endl;
    } else {
        std::cout << "A* did not find a solution." << std::endl;
    }

    std::for_each(search.begin(), search.end(), [&](const auto& item){
        delete item;
    });

    std::for_each(done.begin(), done.end(), [&](const auto& item){
        delete item;
    });
}

int main() {
    std::vector<int> initialState{5, 1, 3, 4, 8, 0, 6, 2, 7};

    AStar(initialState, {1, 2, 3, 4, 5, 6, 7, 8, 0});
    IterativeDeepening(initialState, {1, 2, 3, 4, 5, 6, 7, 8, 0}, 15);
    BreadthFirst(initialState, {1, 2, 3, 4, 5, 6, 7, 8, 0});
}
